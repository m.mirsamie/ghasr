package com.gcom.ghasr;

import android.os.Bundle;
import android.app.Activity;


public class SettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MainActivity.isShown = true;
		setContentView(R.layout.activity_settings);
	}
	@Override
	protected void onPause() {
		super.onPause();
		MainActivity.isShown = false;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		MainActivity.isShown = false;
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		MainActivity.isShown = true;
	}
	@Override
	protected void onStop() {
		super.onStop();
		MainActivity.isShown = false;
	}

}
