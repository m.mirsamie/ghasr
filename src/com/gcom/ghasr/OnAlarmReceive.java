package com.gcom.ghasr;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;


public class OnAlarmReceive extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		//Toast.makeText(context, "timer["+String.valueOf(MainActivity.isShown)+"]", Toast.LENGTH_SHORT).show();
		if(!MainActivity.isShown && !MainActivity.okShown)
		{
			Toast.makeText(context, "لطفا از بستن نرم افزار خودداری کنید\nPlease Do Not CLOSE the Application.", Toast.LENGTH_SHORT).show();
			Intent i = new Intent(context, MainActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		}
		else if(MainActivity.contin)
		{
			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			Intent intent1 = new Intent(context, OnAlarmReceive.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(
				context, 0, intent1,
				PendingIntent.FLAG_UPDATE_CURRENT);		 
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.SECOND, MainActivity.checkTime);
			alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
		}
	}
}
