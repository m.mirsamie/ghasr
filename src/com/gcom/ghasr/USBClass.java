package com.gcom.ghasr;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.FT_Device;

@SuppressLint("HandlerLeak")
public class USBClass {
	public static D2xxManager ftD2xx = null;
	FT_Device ftDevice = null;
	int DevCount = -1;
    EventThread eventThread;
    int iavailable = 0;
    int bufferLength = 21;
    int bufferLength1 = 8;
    Boolean writeCommandRead = true;
    byte[] dataBuffer = new byte[bufferLength];
    byte[] totalBuffer = new byte[bufferLength];
    Boolean threadBussy = false;
    Boolean continiueReading = false;
    int lastIndex = 0;
    Context mContext;
    Boolean packageRec = false;
    Boolean isActive = false;
    String err = "";
    int totalRead = 0;
    long CRC16=0;
    Boolean crcok = false;

	Handler EventThread=new Handler() {
    	@Override
    	public void handleMessage(Message msg)
    	{
    	}
	};
    
    class EventThread extends Thread {
    	Handler mHandler;
    	
    	EventThread(Handler h){
			this.setPriority(Thread.MAX_PRIORITY);
		}
    	
    	@Override
		public void run() {
    		while(continiueReading)
    		{
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
				}
				iavailable = ftDevice.getQueueStatus();
				if (iavailable > 0) 
				{
					int toRead = -1;
					if(writeCommandRead)
						toRead = bufferLength - lastIndex;
					else						
						toRead = bufferLength1 - lastIndex;
					if(toRead>0)
					{
						if(iavailable > toRead)
							iavailable = toRead;
						totalRead += iavailable;
						ftDevice.read(dataBuffer,iavailable);
						for(int i = 0;i < iavailable;i++)
							totalBuffer[lastIndex+i] = dataBuffer[i];
						lastIndex += iavailable;
						if((lastIndex == bufferLength && writeCommandRead)||(lastIndex == bufferLength1 && !writeCommandRead))
						{
							crcok = dataValidate(bytesToInts(totalBuffer), ((writeCommandRead)?bufferLength:bufferLength1));
							writeCommandRead = !writeCommandRead;
							packageRec = true;
						}
					}
					else
					{
						crcok = dataValidate(bytesToInts(totalBuffer), ((writeCommandRead)?bufferLength:bufferLength1));
						writeCommandRead = !writeCommandRead;
						packageRec = true;
					}
				}
				
    		}
			threadBussy = false;
    	}
    }
    
    public int[] modBusTwist(int[] inp,int start)
    {
    	int[] out = null;
    	try {			
        	out = new int[inp.length];
        	for(int j = 0;j < start;j++)
        		out[j] = inp[j];
        	for(int i = start;i < inp.length;i+=2)
        	{
        		if(i+1 < inp.length)
        		{
        			out[i] = inp[i+1];
        			out[i+1] = inp[i];
        		}
        		else
        			out[i] = inp[i];
        	}
		} catch (Exception e) {
		}
    	return out;
    }
    
    public int[] modBusTwist(int[] inp,int start,int end)
    {
    	int[] out = null;
    	try {			
        	out = new int[inp.length];
        	for(int j = 0;j < start;j++)
        		out[j] = inp[j];
        	for(int i = start;i < end;i+=2)
        	{
        		if(i+1 < end)
        		{
        			out[i] = inp[i+1];
        			out[i+1] = inp[i];
        		}
        		else
        			out[i] = inp[i];
        	}
        	for(int j = end;j < inp.length;j++)
        		out[j] = inp[j];
        	
		} catch (Exception e) {
		}
    	return out;
    }

    public USBClass(Context inContext)
    {
    	mContext = inContext;
    }
    
	public static String bytesToStr(byte[] inp,Boolean toChar)
	{
		String out= "";
		for(int i = 0;i < inp.length;i++)
			out += ((out != "")?",":"")+((toChar)?(char)inp[i]:String.valueOf(inp[i]));
		return out;
	}

	public static String intsToStr(int[] inp,Boolean toChar)
	{
		String out= "";
		for(int i = 0;i < inp.length;i++)
			out += ((out != "")?",":"")+((toChar)?(char)inp[i]:String.valueOf(inp[i]));
		return out;
	}
	
	public void startApp()
	{
    	try {
    		ftD2xx = D2xxManager.getInstance(mContext);
    		conn();
    		err = "";
    	} catch (D2xxManager.D2xxException ex) {
    		err += ex.getMessage()+"\n";
    	}

	}
	
	public int[] bytesToInts(byte[] inp)
	{
		int[] out = new int[inp.length];
		for(int i = 0;i < inp.length;i++)
			out[i] = inp[i]+((inp[i]<0)?256:0);
		return out;
	}
	public byte[] intsToBytes(int[] inp)
	{
		byte[] out = new byte[inp.length];
		for(int i = 0;i < inp.length;i++)
			out[i] = (byte) inp[i];
		return out;
	}

	public void conn()
	{
		try {
			
			int openIndex = 0;
			isActive = true;
			if (DevCount > 0)
				return;
			DevCount = ftD2xx.createDeviceInfoList(mContext);
			if (DevCount > 0) {
				ftDevice = ftD2xx.openByIndex(mContext, openIndex);
				if(ftDevice == null)
				{
					isActive = false;
					err += "ftDevice = null\n";
					return;
				}
				if (true == ftDevice.isOpen())
				{
					ftDevice.setBitMode((byte) 0, D2xxManager.FT_BITMODE_RESET);
					ftDevice.setBaudRate(57600);
					ftDevice.setDataCharacteristics(D2xxManager.FT_DATA_BITS_8,
					D2xxManager.FT_STOP_BITS_1, D2xxManager.FT_PARITY_NONE);
					ftDevice.setFlowControl(D2xxManager.FT_FLOW_NONE, (byte) 0x00, (byte) 0x00);
					ftDevice.setLatencyTimer((byte) 16);
					ftDevice.purge((byte) (D2xxManager.FT_PURGE_TX | D2xxManager.FT_PURGE_RX));
					ftDevice.purge((byte) (D2xxManager.FT_PURGE_TX));
					ftDevice.restartInTask();
					startReading();
				}
				else
				{
					err += "ftDevice.isOpen = false\n";
					isActive = false;
				}
			}
			else
			{
				err += "devCount = 0\n";
				isActive = false;
			}
		} catch (Exception e) {
			isActive = false;
		}
	}
	
	public void startReading()
	{
		continiueReading = true;
   		eventThread = new EventThread(EventThread);
   		threadBussy = true;
		eventThread.start();
	}
	
	public void stopReading()
	{
		continiueReading = false;
	}
	
	public void flushBuffer()
	{
		totalBuffer = new byte[bufferLength];
		lastIndex = 0;
		packageRec = false;
	}
	
	public void writeData()
	{
		if(ftDevice != null)
			ftDevice.write(totalBuffer, bufferLength);
		else
			isActive = false;
	}
	
	public void writeData(byte[] inp)
	{
		if(ftDevice != null)
			ftDevice.write(inp, inp.length);
		else
			isActive = false;
	}
	public static long CRCCheck(int[] by)
	{
        long crc16,Temp;
        long crc = 65535;
        for(int i = 0;i < by.length;i++)
        {
                Temp = crc;
                crc = (int) (Temp ^ by[i]);
                for(int j = 0;j < 8;j++)
                {
                        if((crc & 1)==1)
                                crc = ((crc >> 1) ^ 40961);
                        else
                                crc = (crc >> 1);
                }
        }
        crc16 = crc & 65535;
        return crc16;
	}
	public static int[] longToBytes(long x) {
		int[] out = new int[2];
		out[0] = (int)(x % 256);
		out[1] = (int)(x / 256);
		return(out);
	}
	public static int unsignedByte(byte b)
	{
		int out;
		out = (b < 0)?b+256:b;
		return out;
	}
	public long bytesToLong(int[] b)
	{
		long out = 0;
		if(b.length == 2)
			out = b[1]*256+b[0];
		return out;
	}
	public Boolean dataValidate(int[] sts,int bfs)
	{
		Boolean out = false;
		int[] tmp = new int[bfs-2];
		int[] crc = new int[2];
		for(int i = 0;i < (bfs-2);i++)
			tmp[i] = sts[i];
		crc[0] = sts[bfs-2];
		crc[1] = sts[bfs-1];
		CRC16 = CRCCheck(tmp);
		out = (bytesToLong(crc)==CRC16);
		return out;
	}
	public String byteArrayToString(byte[] inp)
	{
		String outStr = "";
		for(int strI = 0;strI < inp.length;strI++)
			outStr += ((outStr!="")?",":"")+String.valueOf(inp[strI]);
		return(outStr);
	}
	public String intArrayToString(int[] inp)
	{
		String outStr = "";
		for(int strI = 0;strI < inp.length;strI++)
			outStr += ((outStr!="")?",":"")+String.valueOf(inp[strI]);
		return(outStr);
	}


}
