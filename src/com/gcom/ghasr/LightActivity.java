package com.gcom.ghasr;

import android.os.Build;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;

public class LightActivity extends Activity {
	WebView mywebview;
	static Boolean active = false;

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_light);
		mywebview = (WebView) findViewById(R.id.wl3);
		WebSettings webSettings = mywebview.getSettings();
		mywebview.addJavascriptInterface(new LightJSClass(this), "LightJSClass");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/light/light.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
	}
	

	@Override
	protected void onPause() {
		super.onPause();
		active = false;
		MainActivity.okShown = false;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		active = false;
		MainActivity.okShown = false;
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		active = false;
		MainActivity.okShown = false;
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		active = true;
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		active = true;
	}

}
