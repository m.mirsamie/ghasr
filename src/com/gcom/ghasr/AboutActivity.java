package com.gcom.ghasr;


import android.os.Bundle;
import android.widget.TextView;
import android.app.Activity;


public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MainActivity.isShown = true;
		setContentView(R.layout.activity_about);
		TextView t1 = (TextView)findViewById(R.id.t1);
		t1.setText(MainActivity.sent);
	}
	@Override
	protected void onPause() {
		super.onPause();
		MainActivity.okShown = false;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		MainActivity.okShown = false;
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		MainActivity.okShown = false;
	}

}
