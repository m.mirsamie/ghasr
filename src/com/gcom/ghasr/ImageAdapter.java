package com.gcom.ghasr;

import java.util.concurrent.Callable;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setBackgroundColor(Color.parseColor("#000000"));
            imageView.setPadding(10, 10, 10, 10);
            imageView.setLayoutParams(new GridView.LayoutParams(100, 100));
            imageView.setPadding(0, 0, 0, 0);
            imageView.setContentDescription(String.valueOf(position));
            imageView.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View arg0) {
					if(activities[Integer.valueOf(String.valueOf(arg0.getContentDescription()))]!=null)
					{
						MainActivity.okShown = true;
						Intent intent = new Intent();
						intent.setClass(mContext, activities[Integer.valueOf(String.valueOf(arg0.getContentDescription()))]);
						try {
							mContext.startActivity(intent);
						} catch (Exception e) {
							//Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
						}
					}
					else
					{
						int pos = Integer.valueOf(String.valueOf(arg0.getContentDescription()));
						if(pos == 5)
						{
							//Toast.makeText(mContext, "isDo "+String.valueOf(MainActivity.isDoNot), Toast.LENGTH_SHORT).show();
							/*
							if(!MainActivity.isDoNot && MainActivity.isRoomSer)
							{
								Toast.makeText(mContext, "set roomser false", Toast.LENGTH_SHORT).show();
								MainActivity.allData.updateRoomService(false);
							}
							*/
							MainActivity.allData.updateDoNotDisturbe(!MainActivity.isDoNot);
						}
						else if(pos == 6)
						{
							//Toast.makeText(mContext, "isRo "+String.valueOf(MainActivity.isRoomSer), Toast.LENGTH_SHORT).show();
							/*
							if(!MainActivity.isRoomSer && MainActivity.isDoNot)
							{
								Toast.makeText(mContext, "set donot false", Toast.LENGTH_SHORT).show();
								MainActivity.allData.updateDoNotDisturbe(false);
							}
							*/
							MainActivity.allData.updateRoomService(!MainActivity.isRoomSer);
						}
						else if(pos == 7)
						{
							final ResturanJsClass rs = new ResturanJsClass(mContext);
							rs.startCommand(3, new Callable<Void>() {
								
								@Override
								public Void call() throws Exception {
									if(ResturanJsClass.orderDone=="true")
									{
										MainActivity.statChanged=true;
										MainActivity.isLandry = !MainActivity.isLandry;
									}
									return null;
								}
							});
							//MainActivity.(!MainActivity.isRoomSer);
						}
						else if(pos == 8)
						{
							final ResturanJsClass rs = new ResturanJsClass(mContext);
							rs.startCommand(4, new Callable<Void>() {
								
								@Override
								public Void call() throws Exception {
									if(ResturanJsClass.orderDone=="true")
									{
										MainActivity.statChanged=true;
										MainActivity.isFix = !MainActivity.isFix;
									}
									return null;
								}
							});
							//MainActivity.(!MainActivity.isRoomSer);
						}
						else if(pos == 9)
						{
							final ResturanJsClass rs = new ResturanJsClass(mContext);
							rs.startCommand(7, new Callable<Void>() {
								
								@Override
								public Void call() throws Exception {
									if(ResturanJsClass.orderDone=="true")
									{
										MainActivity.statChanged=true;
										MainActivity.isEmergency = !MainActivity.isEmergency;
									}
									return null;
								}
							});
							//MainActivity.(!MainActivity.isRoomSer);
						}
					}
				}
			});
        } else {
            imageView = (ImageView) convertView;
        }
        if(position <5 || position>9)
        	imageView.setImageResource(mThumbIds[position]);
        else
        {
        	//Toast.makeText(mContext, "pos = "+String.valueOf(position), Toast.LENGTH_SHORT).show();
        	if(position == 5)
        		imageView.setImageResource(MainActivity.isDoNot?R.drawable.dnd_true:R.drawable.dnd_false);
        	if(position == 6)
        		imageView.setImageResource(MainActivity.isRoomSer?R.drawable.rs_true:R.drawable.rs_false);
        	if(position == 7)
        		imageView.setImageResource(MainActivity.isLandry?R.drawable.landry:R.drawable.unlandry);
        	if(position == 8)
        		imageView.setImageResource(MainActivity.isFix?R.drawable.fix:R.drawable.unfix);
        	if(position == 9)
        		imageView.setImageResource(MainActivity.isEmergency?R.drawable.emergency:R.drawable.unemergency);
        }
        return imageView;
    }

    private Integer[] mThumbIds = {
            R.drawable.lights,R.drawable.scenario, R.drawable.bill,
            R.drawable.food,R.drawable.coffee,
            R.drawable.dnd_false,R.drawable.rs_false,
            R.drawable.landry,R.drawable.fix,
            R.drawable.emergency
    };
    @SuppressWarnings({ "rawtypes" })
	private Class[] activities = {
	    	LightActivity.class,ScenarioActivity.class,BillActivity.class
			,ResturanActivity.class,CofeeshopActivity.class,
			null,null,null,null,null
			
	};
}