package com.gcom.ghasr;

import java.util.concurrent.Callable;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.webkit.JavascriptInterface;
import android.widget.Toast;


public class ResturanJsClass {
	
	Context mcontext;
	MasterTcpClass m;
	public Boolean ready_resturan=true;
	public Boolean ready_order=true;
	public static String[][] menu_list ;
	public static String jsfn="";
	public static int connectPeriod = 100;
	public static String orderData="";
	public static String orderDone = "false";
	public static int commandNum=0;
	public static Callable<Void> callableFn;
	int coffeeshopCode = 1;
	int coffeeshopOrder = 9;
	int resturantCode = 2;
	int resturantOrder = 10;
	int landryCode = 3;
	int roomserviceCode = 4;
	int donotdisCode = 5;
	int serviceCode = 6;
	int policeCode = 7;
	int factorCode = 8;
	int okStat = 1;
	int nokStat = 0;
	int clientId = 195;
	String hostAddr = "192.168.1.3";
	int port = 8080;
	
	public ResturanJsClass(Context conte) {
		mcontext = conte;
		m = new MasterTcpClass(mcontext, hostAddr, port, clientId);
		m.open();

	}
	

	
	//----------------------------JAVAScript Interfaces-----------------------------
	@JavascriptInterface
	public void startReadClient(Callable<Void> jsCallBack)
	{
		callableFn = jsCallBack;
		ready_resturan=false;
		new Thread(new MasterClientObjectThread()).start();
	    final Handler handler = new Handler();
	    final Runnable r = new Runnable()
	    {
	        public void run() 
	        {
	        	try {
	        		if(!ready_resturan)
	        			handler.postDelayed(this, connectPeriod);
	        		else
	        			clientRead();
				} catch (Exception e) {
				}
	        }
	    };
	    if(!ready_resturan)
	    	handler.postDelayed(r, connectPeriod);

		
	}

	
	@JavascriptInterface
	public void startReadResturan(String jsCallBack)
	{
		jsfn = jsCallBack;
		ready_resturan=false;
		new Thread(new MasterClientThread()).start();
	    final Handler handler = new Handler();
	    final Runnable r = new Runnable()
	    {
	        public void run() 
	        {
	        	try {
	        		if(!ready_resturan)
	        			handler.postDelayed(this, connectPeriod);
	        		else
	        			resturanRead();
				} catch (Exception e) {
				}
	        }
	    };
	    if(!ready_resturan)
	    	handler.postDelayed(r, connectPeriod);

		
	}
	
	@JavascriptInterface
	public void startReadCoffeeShop(String jsCallBack)
	{
		jsfn = jsCallBack;
		ready_resturan=false;
		new Thread(new MasterClientCoffeeShopThread()).start();
	    final Handler handler = new Handler();
	    final Runnable r = new Runnable()
	    {
	        public void run() 
	        {
	        	try {
	        		if(!ready_resturan)
	        			handler.postDelayed(this, connectPeriod);
	        		else
	        			coffeeShopRead();
				} catch (Exception e) {
				}
	        }
	    };
	    if(!ready_resturan)
	    	handler.postDelayed(r, connectPeriod);

		
	}

	
	@JavascriptInterface
	public void startOrderResturan(String orderDat,String jsCallBack)
	{
		jsfn = jsCallBack;
		ready_order=false;
		orderData = orderDat;
		new Thread(new MasterClientRestOrderThread()).start();
	    final Handler handler = new Handler();
	    final Runnable r = new Runnable()
	    {
	        public void run() 
	        {
	        	try {
	        		if(!ready_order)
	        			handler.postDelayed(this, connectPeriod);
	        		else
	        			orderDoneFn();
				} catch (Exception e) {
				}
	        }
	    };
	    if(!ready_order)
	    	handler.postDelayed(r, connectPeriod);
	}
	
	@JavascriptInterface
	public void startOrderCoffeeShop(String orderDat,String jsCallBack)
	{
		jsfn = jsCallBack;
		ready_order=false;
		orderData = orderDat;
		new Thread(new MasterClientCoffeeOrderThread()).start();
	    final Handler handler = new Handler();
	    final Runnable r = new Runnable()
	    {
	        public void run() 
	        {
	        	try {
	        		if(!ready_order)
	        			handler.postDelayed(this, connectPeriod);
	        		else
	        			orderCoffeeDoneFn();
				} catch (Exception e) {
				}
	        }
	    };
	    if(!ready_order)
	    	handler.postDelayed(r, connectPeriod);

		
	}

	@JavascriptInterface
	public void startCommand(int commandNumber,Callable<Void> fn)
	{
		callableFn = fn;
		ready_order=false;
		commandNum = commandNumber;
		new Thread(new MasterClientCommandThread()).start();
	    final Handler handler = new Handler();
	    final Runnable r = new Runnable()
	    {
	        public void run() 
	        {
	        	try {
	        		if(!ready_order)
	        			handler.postDelayed(this, connectPeriod);
	        		else
	        			sendCommandEnd();
				} catch (Exception e) {
				}
	        }
	    };
	    if(!ready_order)
	    	handler.postDelayed(r, connectPeriod);

		
	}
	
	@JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mcontext, toast, Toast.LENGTH_SHORT).show();
    }
	
	@JavascriptInterface
	public String showList()
	{
		String tmpall = "";
		for(int i = 0;i < menu_list.length;i++)
		{
			String tmp = "";
			for(int j = 0;j < menu_list[i].length;j++)
				tmp+= ((tmp!="")?",":"")+ menu_list[i][j];
			tmpall += ((tmpall!="")?"|":"")+tmp;
		}
		return(tmpall);
	}
	
	@JavascriptInterface
	public String showError()
	{
		return m.SockError;
	}
	//---------------------------------CallBacks--------------
	public void sendCommandEnd()
	{
		try {
			callableFn.call();
		} catch (Exception e) {
			
		}
	}
	
	@SuppressLint("NewApi")
	public void resturanRead()
	{
		try {
			if(jsfn!="")
			{
				final ResturanActivity ra =  (ResturanActivity) mcontext;
				ra.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						ra.execJavaScript("javascript:"+jsfn+"('"+showList()+"');");					}
				});
			}
		} catch (Exception e) {
			Toast.makeText(mcontext,e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	public void coffeeShopRead()
	{
		try {
			if(jsfn!="")
			{
				final CofeeshopActivity ra =  (CofeeshopActivity) mcontext;
				ra.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						ra.execJavaScript("javascript:"+jsfn+"('"+showList()+"');");					}
				});
			}
		} catch (Exception e) {
			Toast.makeText(mcontext,e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	public void orderDoneFn()
	{
		try {
			if(jsfn!="")
			{
				final ResturanActivity ra =  (ResturanActivity) mcontext;
				ra.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						ra.execJavaScript("javascript:"+jsfn+"('"+orderDone+"');");					}
				});
			}
		} catch (Exception e) {
			Toast.makeText(mcontext,e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	public void orderCoffeeDoneFn()
	{
		try {
			if(jsfn!="")
			{
				final CofeeshopActivity ra =  (CofeeshopActivity) mcontext;
				ra.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						ra.execJavaScript("javascript:"+jsfn+"('"+orderDone+"');");					}
				});
			}
		} catch (Exception e) {
			Toast.makeText(mcontext,e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	
	public void clientRead()
	{
		try {
			callableFn.call();
		} catch (Exception e) {
		}
	}
	
	//---------------------------Threads----------------------
	class MasterClientThread implements Runnable {
		@Override
		public void run() {
			try {
				if(!m.connected)
					m.open();
				if(m.connected)
					menu_list = m.getResturantMenu();
			} catch (Exception e) {
			}
			ready_resturan=true;
		}
	}
	
	class MasterClientObjectThread  implements Runnable {
		@Override
		public void run() {
			try {
				if(!m.connected)
					m.open();
				if(m.connected)
					orderDone = m.getClientObject();
			} catch (Exception e) {
			}
			ready_resturan=true;
		}
	}
	
	class MasterClientCoffeeShopThread implements Runnable {
		@Override
		public void run() {
			try {
				if(!m.connected)
					m.open();
				if(m.connected)
					menu_list = m.getCoffeeShopMenu();
			} catch (Exception e) {
			}
			ready_resturan=true;
		}
	}

	class MasterClientRestOrderThread implements Runnable {
		@Override
		public void run() {
			try {
				if(!m.connected)
					m.open();
				if(m.connected)
				{
		        	orderDone = m.sendOrder(orderData, resturantOrder);
				}
			} catch (Exception e) {
			}
			ready_order=true;
		}
	}

	class MasterClientCoffeeOrderThread implements Runnable {
		@Override
		public void run() {
			try {
				if(!m.connected)
					m.open();
				if(m.connected)
					orderDone = m.sendOrder(orderData, coffeeshopOrder);
			} catch (Exception e) {
			}
			ready_order=true;
		}
	}

	class MasterClientCommandThread implements Runnable {
		@Override
		public void run() {
			try {
				if(!m.connected)
					m.open();
				if(m.connected)
					orderDone = m.sendCommand(commandNum);
			} catch (Exception e) {
			}
			ready_order=true;
		}
	}
}
