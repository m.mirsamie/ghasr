package com.gcom.ghasr;


import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.view.Menu;
import android.webkit.WebSettings;
import android.webkit.WebView;

	
@SuppressLint("SetJavaScriptEnabled")
public class CofeeshopActivity extends Activity {
	public static WebView mywebview;
	static Boolean active = false;
	
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cofeeshop);
		mywebview = (WebView) findViewById(R.id.w_cofee);
		WebSettings webSettings = mywebview.getSettings();
		mywebview.addJavascriptInterface(new ResturanJsClass(this), "ResturanJsClass");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/cofeeshop/cofeeshop.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cofeeshop, menu);
		return true;
	}
	@SuppressLint("NewApi")
	public void execJavaScript(String jfn)
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			mywebview.evaluateJavascript(jfn, null);
		}
		else
			mywebview.loadUrl(jfn);
	}

}
