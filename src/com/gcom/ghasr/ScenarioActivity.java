package com.gcom.ghasr;

import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.webkit.WebSettings;
import android.webkit.WebView;

@SuppressLint("SetJavaScriptEnabled") public class ScenarioActivity extends Activity {
	WebView mywebview;

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scenario);
		mywebview = (WebView) findViewById(R.id.w4);
		WebSettings webSettings = mywebview.getSettings();
		mywebview.addJavascriptInterface(new LightJSClass(this), "LightJSClass");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/scenario/scenario.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		MainActivity.okShown = false;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		MainActivity.okShown = false;
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		MainActivity.okShown = false;
	}
}
