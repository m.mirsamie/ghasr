package com.gcom.ghasr;

import android.os.Bundle;
import android.provider.Settings.Secure;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class InValid extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_in_valid);
		final String android_id = Secure.getString(getBaseContext().getContentResolver(),
                Secure.ANDROID_ID);
		TextView tx = (TextView) findViewById(R.id.iad);
		tx.setText(String.valueOf(android_id));
		final Button bt1 = (Button) findViewById(R.id.active_exit);
		Button bt = (Button) findViewById(R.id.active_bt);
		bt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EditText edt = (EditText) findViewById(R.id.active_edt);
				String tmp = edt.getText().toString();
				int tt =Integer.parseInt(android_id.substring(0,6), 16);
				tt = (tt-1361)*666;
				String last = String.valueOf(Integer.toHexString(tt));
				if(tmp.equals(last))
				{
					Validate valid=new Validate(InValid.this);
					valid.open();
					valid.emptyTable();
					valid.addConf("serial", last);
					valid.close();
					TextView tx1 = (TextView) findViewById(R.id.ac_msg);
					tx1.setText("با تشكر فعال سازي با موفقيت انجام شد لطفا نرم افزار را دوباره  اجرا نماييد");
					arg0.setVisibility(View.GONE);
					bt1.setVisibility(View.VISIBLE);
				}
				else
					Toast.makeText(getBaseContext(), "كد فعال سازي اشتباه  وارد شده است", Toast.LENGTH_LONG).show();
			}
		});
		bt1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.in_valid, menu);
		return true;
	}

}
