package com.gcom.ghasr;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class msqlite extends SQLiteOpenHelper
{
	static final String CONF_TABLE_NAME = "conf";
	private static final String CONF_TABLE_CREATE = "create table " + CONF_TABLE_NAME + " (id integer primary key,cKey text,cValue text);";
	private static final String DATABASE_NAME = "products.db";
	private static final int DATABASE_VERSION = 1;
	public msqlite(Context context) {
		super(context,DATABASE_NAME,null,DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CONF_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

}
