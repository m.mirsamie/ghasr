package com.gcom.ghasr;

import android.content.Context;

public class DataCompiler 
{
	public byte[] input;
	Context mContext;
	byte[] sendDataValue = {1,1};
	Boolean dataPresented = false;
	Boolean offline = true;	
	DataBlockClass[] dataBlockClass;
	int memOutIndex = 13;
	int memInIndex  = 9;
	int memMisIndex = 5;
	//--------------------Dimmers----------------------
	LightsClass[] allLights;
	LightClass[] lights;
	int lightStartIndex = 0;
	int lightCount = 0;
	String[] lightNames = {};
	//-------------------------------------------------
	//---------------------Reles-----------------------
	RelesClass[] allReles;
	ReleClass[] reles;
	int releStartIndex = 0;
	int releCount = 15;
	String[] releNames = {"R1","R2","R3","R4","R5","R6","R7","R8","R9","R10","R11","R12","R13","R14","R15"};
	//-------------------------------------------------
	//------------------FireSensors--------------------
	FireSensorClass[] fireSensors;
	int fireByte = 1;
	int fireOnBit = 2;
	int fireMuteBit = 3;
	public Boolean fireState = true;
	//-------------------------------------------------
	//------------------MotionSensors------------------
	MotionSensorClass[] motionSensors;
	//-------------------------------------------------
	//------------------AlarmSensors-------------------
	AlarmSensorClass[] alarmSensors;
	int alarmByte = 1;
	int alarmBit = 1;
	public Boolean alarmState = true;
	//-------------------------------------------------
	//------------------Senarios-----------------------
	SenarioClass senario;
	int senarioByteMis = 2;
	//-------------------------------------------------
	//------------------Do Not Disturb-----------------
	int disturbBit = 7;
	//-------------------------------------------------
	//------------------Room Service------------------
	int roomSerBit = 6;
	//-------------------------------------------------
	
	public DataCompiler(Context inContext)
	{
		mContext = inContext;
		releCount = releNames.length;
		dataBlockClass = new DataBlockClass[2];
		int[] releBits1 = {0,1,2,3,4,5,6,7};
		int[] releBits2 = {0,1,2,3,4,5,5};
		dataBlockClass[0] = new DataBlockClass(13, 1, "ReleClass","REL1",releBits1);
		dataBlockClass[1] = new DataBlockClass(15, 1, "ReleClass","REL2",releBits2);
		allReles = new RelesClass[2];
	}
	public int readBit(byte inp,int pos)
	{
		int out = 0;
		int bitMask = (int) Math.pow(2,pos);
		out = ((bitMask & inp) > 0)?1:0;
		return out;
	}
	public byte setBit(byte inp,int pos)
	{
		byte out = inp;
		byte bitMask = (byte) Math.pow(2,pos);
		out = (byte) (bitMask | inp);
		return out;
	}
	public byte resetBit(byte inp,int pos)
	{
		byte out = inp;
		byte bitMask = (byte) Math.pow(2,pos);
		out = (byte) (~bitMask & inp);
		return out;
	}
	public void refreshData()
	{
		try {			
			int j=0;
			int i;
			offline = !MainActivity.connected;
			if(MainActivity.sts.length > 0 && MainActivity.crcok)
			{
				int[] input = MainActivity.sts;
				dataPresented = true;
				int k=0;
				int lightI = 0;
				int lightId = 0;
				int releI = 0;
				int releId = 0;
				int fireSensorId = 0;
				int alarmSensorId = 0;
				int motionSensorId = 0;
				//------------read senario --------------
				byte senarioByte = 6;
				int senarioNumber=0;
				int[] Sts = MainActivity.sts;
				senarioNumber = Sts[senarioByte];
				senario = new SenarioClass(senarioNumber, "سناریو "+ String.valueOf(senarioNumber));
				for(k=0;k < dataBlockClass.length;k++)
				{
					if(dataBlockClass[k].dataClass.equals("LightClass"))
					{
						j = 0;
						int lightStartIndex1 = dataBlockClass[k].StartIndex;
						int lightCount1 = dataBlockClass[k].BlockLength;
						LightClass[] lights1 = new LightClass[lightCount1];
						for(i = lightStartIndex1;i < (lightCount1+lightStartIndex1);i++)
						{
							LightClass tmp = new LightClass(lightId,lightNames[j],input[i]);
							lights1[j] = tmp;
							j++;
							lightId++;
						}
						allLights[lightI] = new LightsClass(dataBlockClass[k].name, lights1);
						lightI++;
					}
					else if(dataBlockClass[k].dataClass.equals("ReleClass"))
					{
						int releStartIndex1 = dataBlockClass[k].StartIndex;
						int releCount1 = dataBlockClass[k].BlockLength;
						int localReleId = 0;
						ReleClass[] reles1 = new ReleClass[dataBlockClass[k].BlockLength*dataBlockClass[k].bits.length];
						//-----------------------------Rele Block------------------------------------------
						for(i = releStartIndex1;i < ((2*releCount1)+releStartIndex1);i+=2)
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte releByte = (byte) input[i];
								byte val = (byte) readBit(releByte,dataBlockClass[k].bits[w]);
								ReleClass tmp = new ReleClass(releId, val,releNames[releId]);
								reles1[localReleId] = tmp;
								releId++;
								localReleId++;
							}
						//---------------------------------------------------------------------------------
						allReles[releI] = new RelesClass(dataBlockClass[k].name, reles1);
						releI++;
					}
					else if(dataBlockClass[k].dataClass.equals("AlarmSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								AlarmSensorClass tmp = new AlarmSensorClass(alarmSensorId, (val == 1));
								alarmSensors[alarmSensorId] = tmp;
							}
						}
						alarmSensorId++;
					}
					else if(dataBlockClass[k].dataClass.equals("FireSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								FireSensorClass tmp = new FireSensorClass(fireSensorId, (val == 1));
								fireSensors[fireSensorId] = tmp;
							}
						}
						fireSensorId++;
					}
					else if(dataBlockClass[k].dataClass.equals("MotionSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								MotionSensorClass tmp = new MotionSensorClass(motionSensorId, (val == 1));
								motionSensors[motionSensorId] = tmp;
							}
						}
						motionSensorId++;
					}
				}
			}
			else
				dataPresented = false;
		} catch (Exception e) {
		}
	}
/*
	public void refreshData(Callable<Void> fn)
	{
		try {			
			int j=0;
			int i;
			offline = !MainActivity.connected;
			if(MainActivity.sts.length > 0 && MainActivity.crcok)
			{
				int[] input = MainActivity.sts;
				dataPresented = true;
				getAlarm();
				int k=0;
				int lightI = 0;
				int lightId = 0;
				int releI = 0;
				int releId = 0;
				int fireSensorId = 0;
				int alarmSensorId = 0;
				int motionSensorId = 0;
				for(k=0;k < dataBlockClass.length;k++)
				{
					if(dataBlockClass[k].dataClass.equals("LightClass"))
					{
						j = 0;
						int lightStartIndex1 = dataBlockClass[k].StartIndex;
						int lightCount1 = dataBlockClass[k].BlockLength;
						LightClass[] lights1 = new LightClass[lightCount1];
						for(i = lightStartIndex1;i < (lightCount1+lightStartIndex1);i++)
						{
							LightClass tmp = new LightClass(lightId,lightNames[j],input[i]);
							lights1[j] = tmp;
							j++;
							lightId++;
						}
						allLights[lightI] = new LightsClass(dataBlockClass[k].name, lights1);
						lightI++;
					}
					else if(dataBlockClass[k].dataClass.equals("ReleClass"))
					{
						int releStartIndex1 = dataBlockClass[k].StartIndex;
						int releCount1 = dataBlockClass[k].BlockLength;
						ReleClass[] reles1 = new ReleClass[dataBlockClass[k].BlockLength*dataBlockClass[k].bits.length];
						//-----------------------------Rele Block------------------------------------------
						for(i = releStartIndex1;i < ((2*releCount1)+releStartIndex1);i+=2)
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte releByte = (byte) input[i];
								byte val = (byte) readBit(releByte,dataBlockClass[k].bits[w]);
								ReleClass tmp = new ReleClass(releId, val,releNames[releId]);
								reles1[releId] = tmp;
								releId++;
							}
						//---------------------------------------------------------------------------------
						allReles[releI] = new RelesClass(dataBlockClass[k].name, reles1);
						releI++;
					}
					else if(dataBlockClass[k].dataClass.equals("AlarmSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								AlarmSensorClass tmp = new AlarmSensorClass(alarmSensorId, (val == 1));
								alarmSensors[alarmSensorId] = tmp;
							}
						}
						alarmSensorId++;
					}
					else if(dataBlockClass[k].dataClass.equals("FireSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								FireSensorClass tmp = new FireSensorClass(fireSensorId, (val == 1));
								fireSensors[fireSensorId] = tmp;
							}
						}
						fireSensorId++;
					}
					else if(dataBlockClass[k].dataClass.equals("MotionSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								MotionSensorClass tmp = new MotionSensorClass(motionSensorId, (val == 1));
								motionSensors[motionSensorId] = tmp;
							}
						}
						motionSensorId++;
					}
				}
			}
			else
				dataPresented = false;
		} catch (Exception e) {
		}
		try {
			fn.call();
		} catch (Exception e) {
			
		}
	}
	*/
	
	public void updateDoNotDisturbe(Boolean inp)
	{
		int releGroupIndex = 1;
		int value = inp?1:0;
		int startMemByte = releStartIndex+releGroupIndex*2+1;
		int tt = dataBlockClass[releGroupIndex].StartIndex;
		int memByte = MainActivity.sts[tt];
		int bitPos = disturbBit;
		byte val;
		if(value == 1)
			val = setBit((byte) memByte,bitPos);
		else
			val = resetBit((byte) memByte,bitPos);
		if(inp)
			val = resetBit((byte) val,roomSerBit);
		int[] outData = new int[5];
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.out;
		outData[2] = startMemByte+((MainActivity.isTcp)?0:memOutIndex-1);
		outData[3] = 1;
		outData[4] = val;
		MainActivity.b = outData;
	}
	public Boolean getDoNotDisturbe()
	{
		int[] input = MainActivity.sts;		
		int alarmb = readBit((byte) input[releStartIndex+memOutIndex+2], disturbBit);
		Boolean out = (alarmb == 1);
		return out;
	}
	public void updateRoomService(Boolean inp)
	{
		int releGroupIndex = 1;
		int value = inp?1:0;
		int startMemByte = releStartIndex+releGroupIndex*2+1;
		int tt = dataBlockClass[releGroupIndex].StartIndex;
		int memByte = MainActivity.sts[tt];
		int bitPos = roomSerBit;
		byte val;
		if(value == 1)
			val = setBit((byte) memByte,bitPos);
		else
			val = resetBit((byte) memByte,bitPos);
		if(inp)
			val = resetBit((byte) val,disturbBit);
		int[] outData = new int[5];
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.out;
		outData[2] = startMemByte+((MainActivity.isTcp)?0:memOutIndex-1);
		outData[3] = 1;
		outData[4] = val;
		MainActivity.b = outData;
	}
	public Boolean getRoomService()
	{
		int[] input = MainActivity.sts;		
		int alarmb = readBit((byte) input[releStartIndex+memOutIndex+2], roomSerBit);
		Boolean out = (alarmb == 1);
		return out;
	}

	
	public void updateReles(int releId,int value,int releGroupIndex)
	{
		try {
			releGroupIndex = (releId>7)?1:0;
			int lastByteReleCount = 0;
			for(int kk = 0;kk < releGroupIndex;kk++)
				if(dataBlockClass[kk].dataClass=="ReleClass")
					lastByteReleCount+=dataBlockClass[kk].bits.length;
			//int bitsC = dataBlockClass[releGroupIndex].bits.length;
			int startMemByte = releStartIndex+releGroupIndex*2+1;//((releId - (releId % bitsC))/bitsC)*2+1;
			int tt = dataBlockClass[releGroupIndex].StartIndex;//((releId - (releId % bitsC))/bitsC)*2;
			int memByte = MainActivity.sts[tt];
			int bitPos = dataBlockClass[releGroupIndex].bits[releId-lastByteReleCount];//dataBlockClass[releGroupIndex].bits[releId % bitsC];
			byte val;
			if(value == 1)
				val = setBit((byte) memByte,bitPos);
			else
				val = resetBit((byte) memByte,bitPos);
			int[] outData = new int[5];
			outData[0] = MainActivity.user_id;
			outData[1] = MainActivity.server_commands.out;
			outData[2] = startMemByte+((MainActivity.isTcp)?0:memOutIndex-1);
			outData[3] = 1;
			outData[4] = val;
			MainActivity.preparingCommand = true;
			MainActivity.b = outData;
		} catch (Exception e) {
			if(MainActivity.writeLogs)
				MainActivity.sent+="\nupdateReles error : "+e.getMessage()+"\n";
		}
	}
	public void updateLights(int lightId,int value,int lightGroupIndex)
	{
		int startMemByte = lightStartIndex+lightId+1;
		int[] outData = new int[5];
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.out;
		outData[2] = startMemByte+((MainActivity.isTcp)?0:memOutIndex);
		outData[3] = 1;
		outData[4] = value;
		MainActivity.b = outData;
	}
	
	public void setAlarm(String inps)
	{
		int[] input = MainActivity.sts;
		int[] outData = new int[5];
		Boolean inp = (inps == "true");
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.mis;
		outData[2] = alarmByte+((MainActivity.isTcp)?0:memMisIndex);
		outData[3] = 1;
		if(inp)
			outData[4] = setBit((byte) input[memMisIndex+alarmByte-1], alarmBit);
		else
			outData[4] = resetBit((byte) input[memMisIndex+alarmByte-1], alarmBit);
		MainActivity.b = outData;
	}
	
	public Boolean getAlarm()
	{
		int[] input = MainActivity.sts;		
		int alarmb = readBit((byte) input[memMisIndex+alarmByte-1], alarmBit);
		Boolean out = (alarmb == 1);
		alarmState = out;
		return out;
	}
	
	public void setFire(String inps)
	{
		Boolean inp = (inps == "true");
		int[] input = MainActivity.sts;
		int[] outData = new int[5];
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.mis;
		outData[2] = fireByte+((MainActivity.isTcp)?0:memMisIndex);
		outData[3] = 1;
		if(inp)
			outData[4] = setBit((byte) input[memMisIndex+fireByte-1], fireOnBit);
		else
			outData[4] = resetBit((byte) input[memMisIndex+fireByte-1], fireOnBit);
		MainActivity.b = outData;
	}
	
	public Boolean getFire()
	{
		int[] input = MainActivity.sts;		
		int alarmb = readBit((byte) input[memMisIndex+alarmByte-1], fireOnBit);
		Boolean out = (alarmb == 1);
		alarmState = out;
		return out;
	}
	
	public void setMuteFire(String inps)
	{
		Boolean inp = (inps == "true");
		int[] input = MainActivity.sts;
		int[] outData = new int[5];
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.mis;
		outData[2] = fireByte+((MainActivity.isTcp)?0:memMisIndex);
		outData[3] = 1;
		if(inp)
			outData[4] = setBit((byte) input[memMisIndex+fireByte-1], fireMuteBit);
		else
			outData[4] = resetBit((byte) input[memMisIndex+fireByte-1], fireMuteBit);
		MainActivity.b = outData;
	}
	
	public Boolean getMuteFire()
	{
		int[] input = MainActivity.sts;		
		int alarmb = readBit((byte) input[memMisIndex+alarmByte-1], fireMuteBit);
		Boolean out = (alarmb == 1);
		alarmState = out;
		return out;
	}
	public void setSenario(int number)
	{
		int[] outData = new int[5];
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.mis;
		outData[2] = senarioByteMis+((MainActivity.isTcp)?0:memMisIndex-1);
		outData[3] = 1;
		outData[4] = number;
		MainActivity.b = outData;
		if(MainActivity.writeLogs)
			MainActivity.sent += "-----------------SET Senaio to "+String.valueOf(number)+" "+USBClass.intsToStr(outData, false)+"\n";
	}
}
