package com.gcom.ghasr;


import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.gcom.ghasr.Conf;
import com.gcom.ghasr.msqlite;

public class Validate {
	private Context mContext;
	private SQLiteDatabase database;
	private msqlite dbHelper;
	private String[] allColumns = {"id","cKey","cValue"};
	public Validate(Context context)
	{
		this.mContext = context;
		dbHelper = new msqlite(context);
	}
	public void addConf(String cKey,String cValue) {
		  ContentValues values = new ContentValues();
		  values.put("cKey", cKey);
		  values.put("cValue", cValue);
		  database.insert(msqlite.CONF_TABLE_NAME, null,values);
	  }
	  public Conf getConf(String key)
	  {
		  Conf out = new Conf(-1,null,null);
		  Cursor cursor = database.query(msqlite.CONF_TABLE_NAME,allColumns, " cKey = '"+key+"' ", null, null, null, null, null);
		  cursor.moveToFirst();
		  if(!cursor.isAfterLast())
		  {
			  out = cursorToConf(cursor);
		  }
		  cursor.close();
		  return out;
	  }
	public void open() throws SQLException {
		  try
		  {
			  database = dbHelper.getWritableDatabase();
		  }catch (Exception e) {
			// TODO: handle exception
			  Toast.makeText(this.mContext, e.getMessage(), Toast.LENGTH_LONG).show();
		  }
	  }
	public void close() {
		 dbHelper.close();
	}
	public void emptyTable()
	{
		 database.execSQL("delete from "+msqlite.CONF_TABLE_NAME+";"); 
	}
	public Conf cursorToConf(Cursor cursor)
	  {
		  int id = (int) cursor.getLong(0);
		  String key = cursor.getString(1);
		  String value = cursor.getString(2);
		  Conf pr = new Conf(id,key,value);
		  return pr;
	  }
	public List<Conf> getList()
	  {
		  List<Conf> out = new ArrayList<Conf>();
		  Cursor cursor = database.query(msqlite.CONF_TABLE_NAME,allColumns, null, null, null, null, "id");
		  cursor.moveToFirst();
		  while (!cursor.isAfterLast()) {
			  Conf newpr = cursorToConf(cursor);
			  out.add(newpr);
			  cursor.moveToNext();
		  }
		  cursor.close();
		  return out;
	  }
}
