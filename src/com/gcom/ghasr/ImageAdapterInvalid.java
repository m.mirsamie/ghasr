package com.gcom.ghasr;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapterInvalid extends BaseAdapter {
	 private Context mContext;

	    public ImageAdapterInvalid(Context c) {
	        mContext = c;
	    }
	    public int getCount() {
	        return 1;
	    }

	    public Object getItem(int position) {
	        return null;
	    }

	    public long getItemId(int position) {
	        return 0;
	    }
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ImageView imageView;
	        if (convertView == null) {
	            imageView = new ImageView(mContext);
	            imageView.setLayoutParams(new GridView.LayoutParams(100, 100));
	            imageView.setPadding(0, 0, 0, 0);
	            imageView.setContentDescription(String.valueOf(position));
	            imageView.setOnClickListener(new OnClickListener() {				
					@Override
					public void onClick(View arg0) {
						if(activities[0]!=null)
						{
							MainActivity.okShown = true;
							Intent intent = new Intent();
							intent.setClass(mContext, activities[0]);
							try {
								mContext.startActivity(intent);
							} catch (Exception e) {
								//Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
							}
						}	
					}
				});
	            imageView.setImageResource(R.drawable.dozd);
	        } else {
	            imageView = (ImageView) convertView;
	        }
	        return imageView;
	    }
	    @SuppressWarnings("rawtypes")
		private Class[] activities = {
		    	InValid.class
		};

}
