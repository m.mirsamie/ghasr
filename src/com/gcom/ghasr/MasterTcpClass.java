package com.gcom.ghasr;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import android.content.Context;

public class MasterTcpClass {
	String SERVER_IP = "192.168.0.174";
	int SERVER_PORT = 43001;
	Boolean connected = false;
	Socket socket;
	Context mContext;
	String SockError="";
	long clientId = 0;
	int coffeeshopCode = 1;
	int coffeeshopOrder = 9;
	int resturantCode = 2;
	int resturantOrder = 10;
	int landryCode = 3;
	int roomserviceCode = 4;
	int donotdisCode = 5;
	int serviceCode = 6;
	int policeCode = 7;
	int factorCode = 8;
	int loadData = 11;
	int okStat = 1;
	int nokStat = 0;
	
	MasterTcpClass(Context inContext,String inServer,int inPort,long inClient)
	{
		mContext = inContext;
		SERVER_IP = inServer;
		SERVER_PORT = inPort;
		clientId = inClient;
	}
	
	public void open()
	{
		if(socket != null)
			connected = socket.isConnected();
		else
			connected = false;
		if(!connected)
		{
			try {
				InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
				socket = new Socket(serverAddr, SERVER_PORT);
				connected = true;
				SockError = "";
			} catch (UnknownHostException e) {
				SockError = "Open() UnknownHostException : "+e.getMessage();
			} catch (IOException e) {
				SockError = "Open() IOException : "+e.getMessage();
			} catch (Exception e) {
				SockError = "Open() Exception : "+e.getMessage();
			}
		}
	}

	public static int[] longToBytes(long x) {
		int[] out = new int[2];
		out[0] = (int)(x % 256);
		out[1] = (int)(x / 256);
		return(out);
	}
	
	public static int unsignedByte(byte b)
	{
		int out;
		out = (b < 0)?b+256:b;
		return out;
	}
	public String byteArrayToString(byte[] inp)
	{
		String outStr = "";
		for(int strI = 0;strI < inp.length;strI++)
			outStr += ((outStr!="")?",":"")+String.valueOf(unsignedByte(inp[strI]));
		return(outStr);
	}
	public String[][] restJson(String inp)
	{
		String[][] out = null;
		String inp1 = inp.replace("[[", "");
		inp1 = inp1.replace("\"", "");
		inp1 = inp1.replace("\\]\\]", "");
		String[] tmp = inp1.split("\\],\\[");
		String[] tmpp = tmp[0].split(",");
		int dim = tmpp.length;
		out = new String[tmp.length][dim];
		for(int i = 0;i < tmp.length;i++)
		{
			out[i] = new String[dim];
			out[i] = tmp[i].split(",");
			for(int j = 0;j < out[i].length;j++)
				out[i][j] = out[i][j].replace("]", "").trim(); 
		}
		return out;
	}
	public String[][] getResturantMenu()
	{
		String[][] out = null;
		try {
			DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
			byte[] cmd = new byte[5];
			int[] clientid = longToBytes(clientId);
			cmd[0] = (byte) clientid[0];
			cmd[1] = (byte) clientid[1];
			cmd[2] = (byte) resturantCode;
			cmd[3] = 0;
			cmd[4] = 0;
			outToServer.write(cmd);
			byte[] resp = new byte[1024];
			DataInputStream inFromServer = new DataInputStream(socket.getInputStream());
			inFromServer.read(resp);
			int dataLength = unsignedByte(resp[3])+unsignedByte(resp[4])*256;
			String jsonstring="";
			byte[] jsonbyte = new byte[dataLength];
			for(int i = 5;i < dataLength+5;i++)
				jsonbyte[i-5] = resp[i];
			jsonstring = new String(jsonbyte,"UTF-8");
			out = restJson(jsonstring);
		} catch (IOException e) {
			SockError += "getResturantMenu() IOException : "+e.getMessage();
		}catch (Exception e) {
			SockError += "getResturantMenu() err "+e.getMessage();
		}
		return out;
	}
	
	public String getClientObject()
	{
		String out = "";
		try {
			DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
			byte[] cmd = new byte[5];
			int[] clientid = longToBytes(clientId);
			cmd[0] = (byte) clientid[0];
			cmd[1] = (byte) clientid[1];
			cmd[2] = (byte) loadData;
			cmd[3] = 0;
			cmd[4] = 0;
			outToServer.write(cmd);
			byte[] resp = new byte[1024];
			DataInputStream inFromServer = new DataInputStream(socket.getInputStream());
			inFromServer.read(resp);
			int dataLength = unsignedByte(resp[3])+unsignedByte(resp[4])*256;
			String jsonstring="";
			byte[] jsonbyte = new byte[dataLength];
			for(int i = 5;i < dataLength+5;i++)
				jsonbyte[i-5] = resp[i];
			jsonstring = new String(jsonbyte,"UTF-8");
			out = jsonstring;
		} catch (IOException e) {
			SockError += "getResturantMenu() IOException : "+e.getMessage();
		}catch (Exception e) {
			SockError += "getResturantMenu() err "+e.getMessage();
		}
		return out;
	}

	
	public String[][] getCoffeeShopMenu()
	{
		String[][] out = null;
		try {
			DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
			byte[] cmd = new byte[5];
			int[] clientid = longToBytes(clientId);
			cmd[0] = (byte) clientid[0];
			cmd[1] = (byte) clientid[1];
			cmd[2] = (byte) coffeeshopCode;
			cmd[3] = 0;
			cmd[4] = 0;
			outToServer.write(cmd);
			byte[] resp = new byte[1024];
			DataInputStream inFromServer = new DataInputStream(socket.getInputStream());
			inFromServer.read(resp);
			int dataLength = unsignedByte(resp[3])+unsignedByte(resp[4])*256;
			String jsonstring="";
			byte[] jsonbyte = new byte[dataLength];
			for(int i = 5;i < dataLength+5;i++)
				jsonbyte[i-5] = resp[i];
			jsonstring = new String(jsonbyte,"UTF-8");
			out = restJson(jsonstring);
		} catch (IOException e) {
			SockError = "getCoffeeShopMenu() IOException : "+e.getMessage();
		}catch (Exception e) {
			SockError = "getCoffeeShopMenu() err "+e.getMessage();
		}
		return out;
	}

	public String sendOrder(String data,int fn)
	{
		String out = "false";
		try {			
			DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
			byte[] bytes = data.getBytes("UTF-8");
			byte[] cmd = new byte[5+bytes.length];
			int[] clientid = longToBytes(clientId);
			cmd[0] = (byte) clientid[0];
			cmd[1] = (byte) clientid[1];
			cmd[2] = (byte) fn;
			int[] dataLenght = longToBytes(bytes.length);
			cmd[3] = (byte) dataLenght[0];
			cmd[4] = (byte) dataLenght[1];
			for(int i = 0;i<bytes.length;i++)
				cmd[5+i] = bytes[i];
			outToServer.write(cmd);
			byte[] resp = new byte[1024];
			DataInputStream inFromServer = new DataInputStream(socket.getInputStream());
			inFromServer.read(resp);
			int dataLength1 = resp[4]+resp[5]*256;
			byte[] jsonbyte = new byte[dataLength1];
			for(int i = 6;i < dataLength1+6;i++)
				jsonbyte[i-6] = resp[i];
			if(dataLength1 ==1 && jsonbyte[0]==1)
				out = "true";
			/*
			String rep_str = "";
			for (int i =0;i<resp.length;i++)
				rep_str +=((rep_str.equals(""))?"":",")+String.valueOf(resp[i]);
			MainActivity.log = "dataLength1:"+String.valueOf(dataLength1)+" jsonbyte:"+String.valueOf(jsonbyte[0])+" resp5:"+resp[5]+"resp4:"+resp[4];
			*/
		} catch (IOException e) {
			SockError = "sendOrder() IOException : "+e.getMessage();
			
		}catch (Exception e) {
			SockError = "sendOrder() err send order "+e.getMessage();
		}
		return out;
	}
	
	public String sendCommand(int command)
	{
		String out = "false";
		try {			
			DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
			byte[] cmd = new byte[5];
			int[] clientid = longToBytes(clientId);
			cmd[0] = (byte) clientid[0];
			cmd[1] = (byte) clientid[1];
			cmd[2] = (byte) command;
			cmd[3] = 0;
			cmd[4] = 0;
			outToServer.write(cmd);
			byte[] resp = new byte[1024];
			DataInputStream inFromServer = new DataInputStream(socket.getInputStream());
			inFromServer.read(resp);
			int dataLength1 = resp[3]+resp[4]*256;
			byte[] jsonbyte = new byte[dataLength1];
			for(int i = 5;i < dataLength1+5;i++)
				jsonbyte[i-5] = resp[i];
			if(dataLength1 ==1 && jsonbyte[0]==1)
				out = "true";
		} catch (IOException e) {
			SockError = "sendOrder() IOException : "+e.getMessage();
			
		}catch (Exception e) {
			SockError = "sendOrder() err send order "+e.getMessage();
		}
		return out;
	}
	
	public String byteArrayToStr(byte[] inp,int ln)
	{
		String out="";
		for(int i = 0;i < ((inp.length<ln)?inp.length:ln);i++)
			out += ((out != "")?",":"")+String.valueOf(unsignedByte(inp[i]));
		return out;
	}

	public String byteArrayToStr(byte[] inp)
	{
		String out="";
		for(int i = 0;i < inp.length;i++)
			out += ((out != "")?",":"")+String.valueOf(inp[i]);
		return out;
	}
	
	public void close()
	{
		try {
			socket.close();
		} catch (IOException e) {
			SockError += "close() : "+e.getMessage();
		}
	}
}
