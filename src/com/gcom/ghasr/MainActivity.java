package com.gcom.ghasr;

import java.util.Calendar;
import java.util.concurrent.Callable;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends Activity {
	static DataCompiler allData;
	//public static String log="";
	private IOClass ioclass;
	private static final int SERVERPORT = 43001;
	private static final String SERVER_IP = "192.168.0.175";
	public class server_commands{
		final static byte read = 1;
		final static byte write = 2;
		final static byte mis = 3;
		final static byte out = 4;
		final static byte dc = 5;
	}
	public static byte user_id = 1;
	public static int bfs = 18;
	public static int bfs1 = 8;
	public static Boolean connected = false;
	public static int[] sts;
	public static int[] b={user_id,server_commands.read};
	public static int[] def_b = {user_id,server_commands.read};
	public static String sent="";
	public static int inputCount = 0;
	public static int connectPeriod = 500;
	public static String SockError = "";
	public static Boolean timerEn = true;
	public static Boolean crcok = false;
	public static Boolean preparingCommand = false;
	public static Boolean socketBussy = false;
	public static Boolean writeLogs = true; 
	public static Calendar exDate;
	public static int packetCounter = 0;
	public static Boolean isTcp = true;
	public static Boolean threadStarted = false;
	public static Boolean isDoNot = false;
	public static Boolean isRoomSer = false;
	public static Boolean isLandry = false;
	public static Boolean isFix = false;
	public static Boolean isEmergency = false;
	public static Boolean statChanged = false;
	USBClass u;
	int slotAddr = 4;
	static Boolean contin = false;
	static Boolean isShown = true;
	static int checkTime = 4;
	static Boolean okShown = false;
	private Boolean isValid=false;
	private Validate valid;

	public static void setupAlarm(int seconds,Context context) {
		MainActivity ma = (MainActivity) context;
		AlarmManager alarmManager = (AlarmManager) ma.getSystemService(ALARM_SERVICE);
		Intent intent = new Intent(context, OnAlarmReceive.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(
			context, 0, intent,
			PendingIntent.FLAG_UPDATE_CURRENT);		 
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, seconds);
		alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		valid = new Validate(MainActivity.this);
		valid.open();
		valid.close();
		//if(conf.id>0)
		isValid= true;
		isShown = true;
		emptyData();
		if(contin)
			setupAlarm(checkTime,this);
	    //drawActivity();
		updateEvents();
	    if(!threadStarted)
	    {
			allData = new DataCompiler(MainActivity.this);
	    	ioclass = new IOClass(SERVER_IP,SERVERPORT,this);
		    ioclass.bfs = bfs;
		    sts = new int[bfs];
		    final Handler handler = new Handler();
		    final Runnable r = new Runnable()
		    {
		        public void run() 
		        {
		        	try {
			        	readData();
			            handler.postDelayed(this, connectPeriod);					
					} catch (Exception e) {
						if(writeLogs)
							sent += "Runnable error\n";
					}
		        }
		    };
		    if(timerEn)
		    	handler.postDelayed(r, connectPeriod);
		    threadStarted = true;
	    }
	}
	public void drawActivity()
	{
	    final GridView gridview = (GridView) findViewById(R.id.gridview);
	    if(isValid)
	    	gridview.setAdapter(new ImageAdapter(this));
	    else
	    	gridview.setAdapter(new ImageAdapterInvalid(this));
	}
	public void emptyData()
	{
		ResturanJsClass.menu_list = new String[0][0];
		ResturanJsClass.orderData="";
		ResturanJsClass.orderDone="false";
	}
	public void readData()
	{

		Boolean lastDoNot = isDoNot;
		Boolean lastRoomSer = isRoomSer;
		isDoNot = allData.getDoNotDisturbe();
		isRoomSer = allData.getRoomService();
		
		if(isDoNot != lastDoNot || isRoomSer != lastRoomSer || statChanged )
		{
			statChanged=false;
			//drawActivity();
			updateEvents();
		}
		if(!socketBussy)
			new Thread(new ClientThread()).start();			
	}
	
	class ClientThread implements Runnable {
		@Override
		public void run() {
			socketBussy = true;
			try {
				ioclass.open();
				int[] tmp_sts;
				if(b[1]!=1)
				{
					tmp_sts = ioclass.sendData(b);
					b = def_b;
				}
				else
					tmp_sts = ioclass.sendData(def_b);
				MainActivity.crcok = ioclass.crcok; 
				if(ioclass.crcok && (b[1] != 0))
				{
					sts = tmp_sts;
					if(writeLogs)
						sent += USBClass.intsToStr(sts, false)+"\n";
				}
				connected = ioclass.connected;
				SockError = ioclass.SockError;
			} catch (Exception e) {
			}
			socketBussy = false;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId())
		{
			case R.id.version:
				PackageInfo pInfo;
				try {
					pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
					Toast.makeText(this, pInfo.versionName, Toast.LENGTH_LONG).show();
				} catch (NameNotFoundException e) {
					Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
				}
				return true;
			case R.id.logview:
				contin = false;
				if(writeLogs)
				{
					writeLogs = false;
					Toast.makeText(this, "لاگ غیرفعال گردید", Toast.LENGTH_LONG).show();
				}
				else
				{
					writeLogs = true;
					Toast.makeText(this, "لاگ فعال گردید", Toast.LENGTH_LONG).show();					
				}
				return true;
			case R.id.about:
				System.exit(0);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		isShown = false;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		isShown = false;
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		isShown = true;
		isDoNot = allData.getDoNotDisturbe();
		isRoomSer = allData.getRoomService();
		emptyData();
		//drawActivity();
		updateEvents();
	}
	@Override
	protected void onStop() {
		super.onStop();
		isShown = false;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		isShown = true;
		isDoNot = allData.getDoNotDisturbe();
		isRoomSer = allData.getRoomService();
		emptyData();
		//drawActivity();
		updateEvents();
	}
	
	@Override
	public void onBackPressed() {
	}
    public void updateEvents()
	{
		final ResturanJsClass rs = new ResturanJsClass(this);
		rs.startReadClient(new Callable<Void>() {
			
			@Override
			public Void call() throws Exception {
				//Toast.makeText(MainActivity.this, ResturanJsClass.orderDone,Toast.LENGTH_SHORT).show();
				String tmp = ResturanJsClass.orderDone.replace("[","");
				tmp = tmp.replace("]", "");
				String[] tmpArr = tmp.split(",");
				if(tmpArr.length==3)
				{
					MainActivity.isLandry = (Integer.valueOf(tmpArr[0].trim())==1);
					MainActivity.isFix = (Integer.valueOf(tmpArr[1].trim())==1);
					MainActivity.isEmergency = (Integer.valueOf(tmpArr[2].trim())==1);
					//Toast.makeText(MainActivity.this,String.valueOf(isLandry)+" "+String.valueOf(isFix)+" "+String.valueOf(isEmergency),Toast.LENGTH_SHORT).show();
				}
				drawActivity();
				return null;
			}
		});	
	}
}

/*
if(!connected)
{
	u.startApp();
	connected = u.isActive;
}
else
{
	if(u.packageRec)
	{
		int[] tmp_sts;
		if(!u.writeCommandRead)
			tmp_sts = new int[u.bufferLength];
		else
			tmp_sts = new int[u.bufferLength1];
		if(u.totalBuffer[1] == server_commands.read)
			tmp_sts = u.modBusTwist(u.bytesToInts(u.totalBuffer),7,19);
		else
			tmp_sts = u.bytesToInts(u.totalBuffer);
		if(tmp_sts[0] == slotAddr)
		{
			if(tmp_sts[1] == server_commands.read)
			{
				sts = tmp_sts;
				crcok =u.crcok;
				if(writeLogs)
					sent += "WRITE("+String.valueOf(crcok)+"): "+USBClass.intsToStr(tmp_sts, false)+"\n";
				if(crcok)
				{
					byte[] tmp_send = new byte[6];
					for(int i = 0;i < 6;i++)
						tmp_send[i] = (byte) tmp_sts[i];
					Long crc16l = USBClass.CRCCheck(u.bytesToInts(tmp_send));
					int[] crc16 = new int[2];
					crc16 = USBClass.longToBytes(crc16l);
					byte[] tmp_send1 = new byte[8];
					for(int i = 0;i < 6;i++)
						tmp_send1[i] = (byte) tmp_send[i];
					tmp_send1[6] = (byte) crc16[0];
					tmp_send1[7] = (byte) crc16[1];
					u.writeData(tmp_send1);
					sent += USBClass.bytesToStr(tmp_send1,false)+"\n";
					isDoNot = allData.getDoNotDisturbe();
					isRoomSer = allData.getRoomService();
					drawActivity();
				}
			}
			else if(tmp_sts[1] == server_commands.write && sts[0] == slotAddr && sts[1] == server_commands.read)
			{
				int[] tmp_sts1 = sts.clone();
				if(b.length >= 5 && b[1] == server_commands.out)
				{
					tmp_sts1[allData.memMisIndex+allData.senarioByteMis-1] = 0; 
					tmp_sts1[b[2]] = b[4];
				}
				else if(b.length >= 5 && b[1] == server_commands.mis)
				{
					tmp_sts1[b[2]] = b[4];						
				}
					
				b[1] = 0;
				byte[] tmp_send = new byte[allData.memInIndex-allData.memMisIndex+3];
				int[] tmp_send1 = new int[allData.memInIndex-allData.memMisIndex+3];
				tmp_send[0] = (byte) slotAddr;
				tmp_send[1] = server_commands.write;
				tmp_send[2] = 8; 
				for(int i = 3;i < tmp_send.length;i++)
					tmp_send[i] = (byte) tmp_sts1[i+allData.memMisIndex-3];
				tmp_send1 = u.modBusTwist(u.bytesToInts(tmp_send), 3);
				Long crc16l = USBClass.CRCCheck(tmp_send1);
				int[] crc16 = new int[2];
				crc16 = USBClass.longToBytes(crc16l);
				int[] tmp_send_final =  new int[allData.memInIndex-allData.memMisIndex+5];
				for(int i = 0;i < tmp_send1.length;i++)
					tmp_send_final[i] = tmp_send1[i];
				tmp_send_final[tmp_send.length] =  crc16[0];
				tmp_send_final[tmp_send.length+1] =  crc16[1];
				u.writeData(u.intsToBytes(tmp_send_final));
				if(writeLogs)
				{
					sent += "READ: "+USBClass.intsToStr(tmp_sts,false)+"\n";
					sent += USBClass.intsToStr(tmp_send_final, false)+"\n";
				}
			}
		}
		else
			u.writeCommandRead = !u.writeCommandRead;
		u.flushBuffer();
	}
}
*/