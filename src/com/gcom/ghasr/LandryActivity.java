package com.gcom.ghasr;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class LandryActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_landry);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.landry, menu);
		return true;
	}
}
